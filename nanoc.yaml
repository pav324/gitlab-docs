# For a list of configuration options, see https://nanoc.ws/doc/reference/config/
string_pattern_type: glob
text_extensions: [
  'adoc',
  'asciidoc',
  'atom',
  'coffee',
  'css',
  'erb',
  'haml',
  'handlebars',
  'hb',
  'htm',
  'html',
  'js',
  'less',
  'markdown',
  'md',
  'ms',
  'mustache',
  'php',
  'rb',
  'rdoc',
  'sass',
  'scss',
  'slim',
  'txt',
  'xhtml',
  'xml']
output_dir: public
content_dir: content
index_filenames: ['index.html']
enable_output_diff: false

domain: docs.gitlab.com
base_url: https://docs.gitlab.com

prune:
  auto_prune: true
  exclude: ['.git', '.hg', '.svn', 'CVS']

data_sources:
  -
    type: filesystem
    items_root: /
    layouts_root: /
    encoding: utf-8
    identifier_type: full

checks:
  internal_links:
    exclude:
      # Exclude major.minor versions that are used under /archives
      - '^/\d{1,2}\.\d{1,2}/'
      # The versions dropdown for the archives is incorrect. Remove this
      # when fixed.
      - '\/archives.html'

  external_links:
    exclude:
      # Valid links incorrectly reported as broken.
      - 'https:\/\/gitlab.com\/api\/v4\/.*'
      - 'https:\/\/www\.linkedin\.com\/company\/gitlab-com'
      - 'https:\/\/auth0\.com\/auth\/login'
      - 'https:\/\/gitlab-org\.gitlab\.io\/gitlab-svgs'
      - 'https:\/\/dl\.acm\.org\/citation\.cfm\?id=361591'
      - 'https:\/\/pgp\.mit\.edu\/pks\/lookup\?op=vindex\&search=.*'
      - 'https:\/\/www\.amazon\.com\/.*'
      - 'https:\/\/contributors\.gitlab\.com\/'
      - 'https:\/\/downloads\.raspberrypi\.org\/raspbian\/images\/'
      - 'https:\/\/wiki\.jenkins\.io\/display\/JENKINS\/'
      - 'https:\/\/twitter\.com\/gitlab'
      # Redirected links to ignore.
      - 'https:\/\/www\.jaegertracing\.io\/docs\/latest\/'
      # Don't check links matching the following patterns.
      - 'https?:\/\/\w*\.example\.com.*'
      - 'https?:\/\/localhost.*'
      - 'https?:\/\/127.0.0.1.*'
      # Don't check gitlab.com links. Cloudflare thinks the link test is a DOS attack.
      # The temporary solution is to exclude them from link testing.
      - 'https:\/\/gitlab.com.*'
      # Some sites rate limit requests, causing 429 errors.
      # The temporary solution is to exclude them from link testing.
      - 'https:\/\/github.com\/.*'
      - 'https:\/\/www.youtube.com\/.*'
      - 'https:\/\/youtu.be\/.*'
      # Some links require authentication, but we want to keep them in development docs
      - 'https:\/\/dashboards.gitlab.com\/.*'

breadcrumbs: true
debug: false

# Control display of survey banner. See README.md#survey-banner
show_banner: true

products:
  ee:
    full_name: 'GitLab'
    short_name: 'GitLab'
    slug: 'ee'
    index_file: 'README.*'
    img: '/assets/images/docs-gitlab.svg'
    description: 'Administer and use self-managed GitLab and GitLab.com'
    repo: 'https://gitlab.com/gitlab-org/gitlab.git'
    dirs:
      temp_dir: 'tmp/ee/'
      dest_dir: 'content/ee'
      doc_dir: 'doc'
    expose: true
    icon: 'tanuki'
  runner:
    full_name: 'GitLab Runner'
    short_name: 'Runner'
    slug: 'runner'
    index_file: 'index.*'
    img: '/assets/images/docs-runner.svg'
    description: 'Install and configure software that runs your CI/CD jobs'
    repo: 'https://gitlab.com/gitlab-org/gitlab-runner.git'
    dirs:
      temp_dir: 'tmp/runner/'
      dest_dir: 'content/runner'
      doc_dir: 'docs'
    expose: true
    icon: 'status'
  omnibus:
    full_name: 'Install GitLab'
    short_name: 'Install GitLab'
    slug: 'omnibus'
    index_file: 'README.*'
    img: '/assets/images/docs-omnibus.svg'
    description: 'Install and configure GitLab by using the Omnibus installer'
    repo: 'https://gitlab.com/gitlab-org/omnibus-gitlab.git'
    dirs:
      temp_dir: 'tmp/omnibus/'
      dest_dir: 'content/omnibus'
      doc_dir: 'doc'
    expose: true
    icon: 'car'
  charts:
    full_name: 'Deploy GitLab'
    short_name: 'Deploy GitLab'
    slug: 'charts'
    index_file: 'index.*'
    img: '/assets/images/docs-charts.svg'
    description: 'Deploy GitLab to Kubernetes by using GitLab Helm Charts'
    repo: 'https://gitlab.com/gitlab-org/charts/gitlab.git'
    dirs:
      temp_dir: 'tmp/charts/'
      dest_dir: 'content/charts'
      doc_dir: 'doc'
    expose: true
    icon: 'chart'
